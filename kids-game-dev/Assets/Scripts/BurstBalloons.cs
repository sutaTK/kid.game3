﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstBalloons : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var balloon = collision.gameObject.GetComponent<Balloon>();
        if (balloon != null)
        {
            balloon.Burst();
        }
    }
}
