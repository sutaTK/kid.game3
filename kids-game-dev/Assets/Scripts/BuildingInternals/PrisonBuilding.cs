﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrisonBuilding : MonoBehaviour, IInteractiveBuilding
{
    public void Action()
    {

        InstantiateRobbers();
    }

    public void InstantiateRobbers()
    {
        var policeman = FindObjectOfType<Level>().character;
        policeman.SetActive(true);
        policeman.GetComponent<CharacterMovement>().Stop();
        policeman.GetComponent<AnimationPoliceman>().Idle();

        var c = FindObjectOfType<CatchingRobbers>();

        c.robbers = new List<GameObject>();
        var i = 0;
        foreach (GameObject o in c.robbersPrefab)
        {
            foreach (CatchingRobbers.ColorType color in c.catchedRobbers)
            {
                Debug.Log(color.ToString() + " | " + o.name);
                if (color.ToString() == o.name)
                {
                    var robber = Instantiate(o, new Vector3(4 - i, -3f, 0), Quaternion.identity);
                    i++;
                    c.robbers.Add(robber);
                    robber.AddComponent<MovableObject>();
                    robber.AddComponent<RobberWindowInteraction>();
                    robber.gameObject.AddComponent<BoxCollider2D>();
                    robber.gameObject.AddComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                    var settings = robber.gameObject.AddComponent<PartSettings>();
                    settings.destionationObjectRangeInstall = 1;
                    settings.destinationObjectTag = "Window";
                    settings.isColorMatching = true;
                }
            }

        }
        policeman.transform.position = new Vector3(6,-2.7f,0);
        c.catchedRobbers.Clear();
        c.car.LeaveCarAll();
        c.car.ClearStars();
    }
}
