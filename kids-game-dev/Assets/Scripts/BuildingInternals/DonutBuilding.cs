﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutBuilding : MonoBehaviour, IInteractiveBuilding
{
    [SerializeField] GameObject donut;
    GameObject character;

    public void Action()
    {
        FindObjectOfType<Car>().DriverLeave();
        character = FindObjectOfType<Level>().character;
        character.SetActive(true);
        character.transform.position = new Vector3(5, -2.7f,0);
        character.GetComponent<AnimationPoliceman>().Idle();
        character.GetComponent<CharacterMovement>().Stop();
        donut.SetActive(true);
    }
}
