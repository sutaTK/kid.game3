﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPoliceman : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void EatDonut()
    {
        animator.Play("eat donut",0);
    }

    public void Idle()
    {
        animator.Play("police idle", 0);
    }

    public void Walk()
    {
        animator.Play("police walk", 0);

    }


}
