﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChangeDay : MonoBehaviour
{
    LevelMover level;
    Image image;
    [SerializeField] Sprite day;
    [SerializeField] Sprite night;

    void Start()
    {
        level = FindObjectOfType<LevelMover>();
        image = GetComponent<Image>();
    }

    public void OnClick()
    {
        if (level.isDay)
            TurnNight();
        else
            TurnDay();
    }

    private void TurnDay()
    {
        if (level.TurnDay(true))
        {
            image.sprite = night;
        }
    }

    private void TurnNight()
    {
        if (level.TurnNight(true))
        {
            image.sprite = day;
        }
    }
}
