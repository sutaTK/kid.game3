﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIActions : MonoBehaviour
{
    WinkerAction winker;
    BeepAction beepAction;
    bool isWrincleActive = false;
    [SerializeField] Image wrinkleButton;
    [SerializeField] Sprite enabledWrinkle;
    [SerializeField] Sprite disabledWrinkle;

    private void Start()
    {
        winker = FindObjectOfType<WinkerAction>();
        beepAction = FindObjectOfType<BeepAction>();
        winker.OnWinkerOff += SetOffSpriteWinker;
    }

    public void WrinkleClicked()
    {
        if (isWrincleActive)
        {
            winker.TurnOff();
            isWrincleActive = false;
            wrinkleButton.sprite = disabledWrinkle;
            
        }
        else
        {
            winker.TurnOn();
            isWrincleActive = true;
            wrinkleButton.sprite = enabledWrinkle;
        }
    }

    public void BeepClicked()
    {
        beepAction.isBlocked = false;
        beepAction.TurnOn();
    }

    private void SetOffSpriteWinker()
    {
        isWrincleActive = false;
        wrinkleButton.sprite = disabledWrinkle;
    }
}
