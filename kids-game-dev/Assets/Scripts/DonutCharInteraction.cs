﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutCharInteraction : MonoBehaviour, IInteractiveAction
{
    public void Action()
    {
        StartCoroutine(ActionWithDelay());
        
    }


    IEnumerator ActionWithDelay()
    {
        var anim = FindObjectOfType<AnimationPoliceman>();
        anim.EatDonut();
        GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(9);
        anim.gameObject.transform.position = new Vector3(0,-10,0);
        FindObjectOfType<Car>().DriverSit();
        var levelMover = FindObjectOfType<LevelMover>();
        levelMover.GetComponent<Level>().audio.Play();
        levelMover.ReturnCashedDay();
        FindObjectOfType<UI>().TurnOnGameplayUI(0);
        levelMover.GetComponent<LevelSpeed>().ReturnAfterStop();
        Destroy(gameObject);
    }
    public void TurnOff()
    {
        throw new System.NotImplementedException();
    }

    public void TurnOn()
    {
        throw new System.NotImplementedException();
    }
}
