﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRobberState 
{
    void CathingState(FillingRobber fillingRobber);
    void FindState(FillingRobber fillingRobber);
    void ReleaseState(FillingRobber fillingRobber);
    void DropState(FillingRobber fillingRobber);
    void DropInCar(FillingRobber fillingRobber);
}
