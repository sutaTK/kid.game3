﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobbersSound : MonoBehaviour
{
    [SerializeField] AudioSource _audioSource;
    [SerializeField] private AudioClip _catchingRobber;
    [SerializeField] private AudioClip _releaseTheRobber;
    [SerializeField] private AudioClip _findRobber;
    [SerializeField] private AudioClip _insideCar;

    public void PlayAudioCatching()
    {
        _audioSource.clip = _catchingRobber;
        _audioSource.Play();
    }
    public void PlayAudioRobberLose()
    {
        _audioSource.clip = _releaseTheRobber;
        _audioSource.Play();
    }
    public void PlayAudioFind()
    {
        _audioSource.clip = _findRobber;
        _audioSource.Play();
    }
    public void InsideCar()
    {
        _audioSource.clip = _insideCar;
        _audioSource.Play();
    }
}
