﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobberFindState : IRobberState
{
    public void CathingState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void ReleaseState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void FindState(FillingRobber fillingRobber)
    {
        fillingRobber.RobbersSound.PlayAudioFind();
        fillingRobber.DragAndDrop.IsCanDrag = true;
    }

    public void DropState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void DropInCar(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
}
