﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobberDropInCarState : IRobberState
{
    public void CathingState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void DropInCar(FillingRobber fillingRobber)
    {
        fillingRobber.RobbersSound.InsideCar();
    }
    public void DropState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void FindState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
    public void ReleaseState(FillingRobber fillingRobber)
    {
        throw new System.NotImplementedException();
    }
}
