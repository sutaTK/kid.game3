﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FillingRobber))]
public class Robber : MonoBehaviour
{
    private IRobberState _state;
    private FillingRobber _fillingRobber;
    private DragAndDrop dragAndDrop;
    private void Start()
    {
        dragAndDrop = FindObjectOfType<DragAndDrop>();
        _fillingRobber = GetComponent<FillingRobber>();
        dragAndDrop.OnFail += Drop;
        dragAndDrop.OnSuccess += DropInCar;
    }

    private void OnDestroy()
    {
        dragAndDrop.OnFail -= Drop;
        dragAndDrop.OnSuccess -= DropInCar;
    }

    public void Catching()
    {
        _state = new CathcState();
        _state.CathingState(_fillingRobber);
    }
    public void Find()
    {
        _state = new RobberFindState();
        _state.FindState(_fillingRobber);
    }
    public void Release()
    {
        _state = new RobberLoseState();
        _state.ReleaseState(_fillingRobber);
    }
    public void Drop()
    {
        _state = new RobberDropState();
       _state.DropState(_fillingRobber);
    }
    public void DropInCar()
    {
        _state = new RobberDropState();
        _state.DropInCar(_fillingRobber);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<LightCatchRobbers>() != null)
        {
            return;
        }   
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<LightCatchRobbers>() != null)
        {
            return;
        }
    }
}
