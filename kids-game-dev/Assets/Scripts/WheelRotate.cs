﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotate : MonoBehaviour
{
    LevelMover level;
    public int rotSpeed;
    [SerializeField] GameObject wheel1;
    [SerializeField] GameObject wheel2;

    private void Start()
    {
        level = FindObjectOfType<LevelMover>();
    }

    void Update()
    {

        wheel1.transform.Rotate(Vector3.back * level.movingSpeed * rotSpeed * Time.deltaTime);
        wheel2.transform.Rotate(Vector3.back * level.movingSpeed * rotSpeed * Time.deltaTime);

    }
}
