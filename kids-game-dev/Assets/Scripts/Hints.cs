﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hints : MonoBehaviour
{
    [SerializeField] GameObject handPrefab;


    DragAndDrop dragAndDrop;
    InputController inputController;
    CarParts carParts;
    DetailsGenerator generator;

    private int numFails;
    private GameObject hand;
    private float timer = 20;

    private void Start()
    {
        hand = Instantiate(handPrefab,Vector3.zero,Quaternion.identity);
        hand.SetActive(false);
        inputController = GetComponent<InputController>();
        generator = FindObjectOfType<DetailsGenerator>();
        carParts = FindObjectOfType<CarParts>();
        dragAndDrop = GetComponent<DragAndDrop>();
        dragAndDrop.OnFail += FailAttempt;
        dragAndDrop.OnSuccess += SuccessAttempt;
        dragAndDrop.OnDoSmth += WhenDoSmth;
    }

    private void Update()
    {
        if(timer <= 0)
        {
            numFails = 0;
            ShowHint();
            timer = 30;

        }

        timer -= Time.deltaTime;
    }

    private void FailAttempt()
    {
        numFails++;
        if (numFails >= 3)
        {
            numFails = 0;
            ShowHint();
        }
    }

    private void SuccessAttempt()
    {
        numFails = 0;
    }

    private void WhenDoSmth()
    {
        timer = 20;
    }


    private void ShowHint()
    {

        inputController.enabled = false;
        var r = Random.Range(0, generator.takedPlaces.Length);
        var detail = generator.GetRandomDetail();
        GameObject posToPlace;
        if (detail == null)
        {
            inputController.enabled = true;
            return;
            detail = FindObjectOfType<PartSettings>()?.gameObject;
            if (detail == null)
                return;
            posToPlace = GameObject.FindGameObjectWithTag(detail.GetComponent<PartSettings>().destinationObjectTag);
        }
        else
        {
         
            posToPlace = carParts.FindCarPartByDetail(detail);
        }
        StartCoroutine(Working(detail.transform, posToPlace.transform ));
    }

    IEnumerator Working(Transform detail,Transform posToPlace)
    {
        hand.SetActive(true);

        for (float i = 0; i<=1.1f;i+= 0.02f)
        {
            hand.transform.position = Vector3.Lerp(Vector3.zero,detail.position,i);
            yield return new WaitForSeconds(0.02f);
        }

        yield return new WaitForSeconds(1f);

        for (float i = 0; i <= 1.1f; i += 0.02f)
        {
            hand.transform.position = Vector3.Lerp(detail.position, posToPlace.position, i);
            yield return new WaitForSeconds(0.02f);
        }

        yield return new WaitForSeconds(1f);
        hand.SetActive(false);
        inputController.enabled = true;

    }
}
