﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsPoliceman : MonoBehaviour
{

    [SerializeField] AudioClip eatSound;

    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayEatSound()
    {
        source.clip = eatSound;
        source.Play();
    }
}
