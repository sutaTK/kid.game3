﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelConnector : MonoBehaviour
{
    [SerializeField] private string path;
    private LevelSettings settings;

    private void Start()
    {
        settings = FindObjectOfType<LevelSettings>();
    }

    public void LoadLevel()
    {
        settings.SetPath(path);
        SceneManager.LoadScene("SampleScene");
    }
}
