﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelWin : MonoBehaviour
{
    Level level;
    PoliceLevel policeLevel;
    BaloonGenerator baloonGenerator;

    private void Awake()
    {
        baloonGenerator = FindObjectOfType<BaloonGenerator>();
        policeLevel = GetComponent<PoliceLevel>();
        level = GetComponent<Level>();

        baloonGenerator.enabled = false;
        baloonGenerator.OnTimeFinihed += Finished;
    }

    private void Finished()
    {

        //FindObjectOfType<UI>().TurnOnGameplayUI();
        //FindObjectOfType<CarSounds>().Unmute();'
        policeLevel.LeavePrison();
        var speed = GetComponent<LevelSpeed>();
        speed.SetSpeedLevel(0, false);
    }

    public void Win()
    {
        //FindObjectOfType<CarSounds>().Mute();
        level.TurnVictoryMusic();
        policeLevel.Init();
        //var catching = GetComponent<CatchingRobbers>();
        //catching.RandomizeColors();

        //FindObjectOfType<UI>().TurnOffGameplayUI();
        baloonGenerator.enabled = true;
    }
}
