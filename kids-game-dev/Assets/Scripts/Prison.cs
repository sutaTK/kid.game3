﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prison : MonoBehaviour
{
    [SerializeField] GameObject placesPool;

    PoliceLevel policeLevel;
    List<GameObject> places;

    private void Start()
    {

        policeLevel = FindObjectOfType<PoliceLevel>();
        Init();
    }

    private void Init()
    {
        for(int i = 0; i < placesPool.transform.childCount; i++)
        {
            
            if (policeLevel.windowPlace.Contains(i))
            {
                placesPool.transform.GetChild(i).tag = "Window";
                var ind = policeLevel.windowPlace.IndexOf(i);
                var place = placesPool.transform.GetChild(i);
                var sr = place.gameObject.AddComponent<SpriteRenderer>();
                var color = policeLevel.windowColor[ind];
                var state = policeLevel.windowState[ind];
                
                Sprite[] arr = new Sprite[1];
                if (state)
                {
                    arr = policeLevel.emptyWindows;
                }
                else
                {
                    arr = policeLevel.filledWindows;
                }

                var index = FindColorIndex(arr, color);
                sr.sprite = arr[index];
            }


        }
    }

    public void FillWindow(GameObject obj)
    {
        var i = policeLevel.windowPlace.IndexOf(int.Parse(obj.name));
        var index = FindColorIndex(policeLevel.filledWindows, policeLevel.windowColor[i]);
        obj.GetComponent<SpriteRenderer>().sprite = policeLevel.filledWindows[index];
        policeLevel.windowState[i] = false;
        
    }

    private int FindColorIndex(Sprite[] arr,CatchingRobbers.ColorType color)
    {
        for(int i =0;i< arr.Length; i++)
        {
            if(arr[i].name == color.ToString())
            {
                return i;
            }
        }
        return 0;
    }
}
