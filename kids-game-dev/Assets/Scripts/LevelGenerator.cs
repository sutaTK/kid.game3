﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] string path;
    [SerializeField] GameObject road;
    //public float passedDistanceFromBackSky;
    public float passedDistanceFromBackCity;
    public float passedDistanceFromBackBuilding;
    public float passedDistanceFromBuilding;
    public float passedDIstanceFromCloud;
    public float nextRoadStuffAfter = 0;

    [SerializeField] private LightCatchRobbers _light;
    [SerializeField] private GameObject _lightGameObject;
    public GameObject[] backCity;
    public GameObject[] backBuildings;
    public GameObject[] backTrees;
    public GameObject[] uniq;
    public GameObject[] simple;
    public GameObject[] trees;
    public GameObject[] bushes;
    public GameObject[] fence;
    public GameObject[] branches;
    public GameObject[] coreBuilding;
    public GameObject[] clouds;
    public GameObject[] roadStuff;

    private Vector3 roadTop; 
    private int nextBuildingWillSpawn;
    private int nextUniqWillSpawn;
    private int nextBackBuildingWillSpawn;
    private int nextRoadStuffWillSpawn;
    private LevelMover levelMover;
    private RandomizeBuldingStuff settingsSimpleBuildings;
    private bool spawNextUniq = false;
    private bool spawNextFakeUniq = false;
    private GameObject fakeUniq;

    public LightCatchRobbers Light { get => _light; set => _light = value; }
    public GameObject LightGameObject { get => _lightGameObject; set => _lightGameObject = value; }

    void Awake()
    {
        _light = _lightGameObject.GetComponentInChildren<LightCatchRobbers>();

        levelMover = GetComponent<LevelMover>();
        settingsSimpleBuildings = GetComponent<RandomizeBuldingStuff>();
        path = FindObjectOfType<LevelSettings>().path + "/Environment";
        Debug.Log(path);

        backCity = Resources.LoadAll<GameObject>(path + "/BackCity");
        backBuildings = Resources.LoadAll<GameObject>(path + "/BackBuildings");
        backTrees = Resources.LoadAll<GameObject>(path + "/BackTrees");
        uniq = Resources.LoadAll<GameObject>(path+"/Unique");
        simple = Resources.LoadAll<GameObject>(path + "/Simple");
        trees = Resources.LoadAll<GameObject>(path + "/Trees");
        bushes = Resources.LoadAll<GameObject>(path + "/Bushes");
        fence = Resources.LoadAll<GameObject>(path + "/Fence");
        branches = Resources.LoadAll<GameObject>(path + "/Branches");
        coreBuilding = Resources.LoadAll<GameObject>(path + "/CoreBuilding");
        clouds = Resources.LoadAll<GameObject>(path + "/Clouds");
        roadStuff = Resources.LoadAll<GameObject>(path + "/RoadStuff");

        AddComponentsToAll();

        roadTop = road.transform.position + Vector3.up * road.GetComponent<SpriteRenderer>().sprite.bounds.size.y / 2;

        SpawnStartEnvironment();
        levelMover.GetComponent<LevelSpeed>().Stop();
        enabled = false;
    }

    private void AddComponentsToAll()
    {
        AddComponents(backCity);
        AddComponents(backBuildings);
        AddComponents(backTrees);
        AddComponents(uniq);
        AddComponents(simple);
        AddComponents(trees);
        AddComponents(bushes);
        AddComponents(fence);
        AddComponents(branches);
        AddComponents(coreBuilding);
        AddComponents(clouds);
        AddComponents(roadStuff);
    }

    private void AddComponents(GameObject[] arr)
    {
        foreach(GameObject obj in arr)
        {
            var rb = obj.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = obj.AddComponent<Rigidbody2D>();
            }
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.interpolation = RigidbodyInterpolation2D.Interpolate;
        }
    }

    void FixedUpdate()
    {
        /*
        if (passedDistanceFromBackCity <= 0)
        {
            var city = Instantiate(backCity[0], transform.position + Vector3.left * 1.65f - Vector3.left * passedDistanceFromBackCity, Quaternion.identity);
            city.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            passedDistanceFromBackCity = city.GetComponent<SpriteRenderer>().sprite.bounds.size.x - 0.2f;  
        }
        */

        if (passedDistanceFromBackBuilding <= 0)
        {
            SpawnBackBuilding();
        }

        if (passedDistanceFromBuilding <= 0)
        {
            if (spawNextFakeUniq)
            {
                var offset = fakeUniq.GetComponent<SpriteRenderer>().bounds.size.x * 1.5f - 2;
                var uniq = Instantiate(fakeUniq, transform.position + Vector3.right * offset, Quaternion.identity);
                var mover = uniq.gameObject.AddComponent<LevelPartMovement>();
                mover.levelMover = levelMover;
                uniq.AddComponent<CarStopper>().levelMover = levelMover;
                mover.parallaxCoef = 1;
                mover.end = new Vector3(-100,mover.end.y, mover.end.z);
                var spriteU = uniq.GetComponent<SpriteRenderer>();
                passedDistanceFromBuilding = offset * 2 +6;
                spawNextFakeUniq = false;
            }
            else
                SpawnFrontBuilding();
        }

        /*
        if(passedDIstanceFromCloud <= 0)
        {
            var rVect = new Vector3(Random.Range(-1, 1), Random.Range(3, 6), 0);
            var cloud = Instantiate(clouds[Random.Range(0, clouds.Length)], transform.position + rVect, Quaternion.identity);
            cloud.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            passedDIstanceFromCloud = Random.Range(1f, 20f);
        }
        */

        //passedDIstanceFromCloud -= levelMover.movingSpeed * levelMover.ParallaxCoef("BackCity") * Time.deltaTime;
        //passedDistanceFromBackCity -= levelMover.movingSpeed * levelMover.ParallaxCoef("BackCity") * Time.deltaTime;
        passedDistanceFromBackBuilding -= levelMover.movingSpeed * levelMover.ParallaxCoef("BackBuildings") * Time.deltaTime;
        passedDistanceFromBuilding -= levelMover.movingSpeed  * Time.deltaTime;

    }

    private void SpawnStartEnvironment()
    {
        var initPos = new Vector3(-14.5f, 4.0f, 0);

        var city = Instantiate(backCity[0], Vector3.zero, Quaternion.identity);
        city.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
        
        var core = Instantiate(coreBuilding[0], initPos , Quaternion.identity);
        core.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
        
    }

    private void SpawnFrontBuilding()
    {

        for (int i = 0; i < 4; i++)
        {
            var sim = Instantiate(simple[nextBuildingWillSpawn], transform.position + Vector3.right * passedDistanceFromBuilding, Quaternion.identity);

            sim.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            sim.gameObject.AddComponent<RandomizeBuldingStuff>().stuff = settingsSimpleBuildings.stuff;
            var spriteC = sim.GetComponent<SpriteRenderer>();
            passedDistanceFromBuilding += spriteC.sprite.bounds.size.x / 2 + 0.3f;

            var betweenPos = transform.position + Vector3.right * passedDistanceFromBuilding;

            var tree = Instantiate(trees[Random.Range(0,trees.Length)],betweenPos,Quaternion.identity);
            var bush = Instantiate(bushes[Random.Range(0, bushes.Length)], betweenPos, Quaternion.identity);
            var fen = Instantiate(fence[Random.Range(0, fence.Length)], betweenPos, Quaternion.identity);

            PutOnRoad(sim);
            PutOnRoad(tree);
            PutOnRoad(bush);
            PutOnRoad(fen);

            tree.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            bush.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            fen.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;

            if(nextRoadStuffAfter == 0)
            {
                var stuff = Instantiate(roadStuff[nextRoadStuffWillSpawn], betweenPos, Quaternion.identity);
                PutOnRoad(stuff);
                stuff.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
                nextRoadStuffAfter = 2;
            }
            nextRoadStuffAfter--;

            nextRoadStuffWillSpawn = RandomExcept(0, roadStuff.Length, nextRoadStuffWillSpawn);
            nextBuildingWillSpawn = RandomExcept(0, simple.Length, nextBuildingWillSpawn);
            if(i<3)
                passedDistanceFromBuilding += simple[nextBuildingWillSpawn].GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 + 0.3f;
            else if(i == 3)
            {
                passedDistanceFromBuilding += uniq[nextUniqWillSpawn].GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 + 0.3f;
            }
        }

        //
        
        var uniqB = Instantiate(uniq[nextUniqWillSpawn], transform.position + Vector3.right * passedDistanceFromBuilding, Quaternion.identity);
        

        if (spawNextUniq)
        {
            //var rob = uniqB.AddComponent<RobbersEnableOnBuilding>();
            //rob.levelGenerator = this;
            uniqB.gameObject.AddComponent<CarStopper>().levelMover = levelMover;
            spawNextUniq = false;
        }

        uniqB.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
        var spriteU = uniqB.GetComponent<SpriteRenderer>();
        passedDistanceFromBuilding += spriteU.sprite.bounds.size.x / 2 + 0.3f;

        var betweenPosU = transform.position + Vector3.right * passedDistanceFromBuilding;

        var treeU = Instantiate(trees[Random.Range(0, trees.Length)], betweenPosU, Quaternion.identity);
        var bushU = Instantiate(bushes[Random.Range(0, bushes.Length)], betweenPosU, Quaternion.identity);
        var fenU = Instantiate(fence[Random.Range(0, fence.Length)], betweenPosU, Quaternion.identity);

        PutOnRoad(uniqB);
        PutOnRoad(treeU);
        PutOnRoad(bushU);
        PutOnRoad(fenU);

        treeU.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
        bushU.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
        fenU.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;

        if (nextRoadStuffAfter == 0)
        {
            var stuff = Instantiate(roadStuff[nextRoadStuffWillSpawn], betweenPosU, Quaternion.identity);
            PutOnRoad(stuff);
            stuff.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;
            nextRoadStuffAfter = 2;
        }
        nextRoadStuffAfter--;

        nextUniqWillSpawn = RandomExcept(0, uniq.Length, nextUniqWillSpawn);
        passedDistanceFromBuilding += simple[nextBuildingWillSpawn].GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 + 0.3f;
    }

    private void SpawnBackBuilding()
    {
        var back = Instantiate(backBuildings[nextBackBuildingWillSpawn], transform.position, Quaternion.identity);
        back.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;

        var tree = Instantiate(backTrees[Random.Range(0, backTrees.Length)], transform.position + Vector3.down +
                               Vector3.right * (back.GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 + 0.3f), Quaternion.identity);
        tree.gameObject.AddComponent<LevelPartMovement>().levelMover = levelMover;

        nextBackBuildingWillSpawn = RandomExcept(0, backBuildings.Length, nextBackBuildingWillSpawn);
        passedDistanceFromBackBuilding = back.GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 +
                                         backBuildings[nextBackBuildingWillSpawn].GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2 + 0.6f;
    }

    public void SpawnExceptUniq(string[] except)
    {
        spawNextUniq = true;
        nextUniqWillSpawn = RandomExceptName(0,uniq.Length,nextUniqWillSpawn,except,uniq);
    }

    public void SpawnConcreteUniq(string building)
    {
        spawNextUniq = true;
        for (int i = 0;i<uniq.Length;i++)
        {
            if (uniq[i].name == building)
            {

                nextUniqWillSpawn = i;
            }
        }
    }

    public void SpawnAsUniq(string path)
    {
        spawNextFakeUniq = true;
        fakeUniq = (Resources.Load<GameObject>(path));
    }

    private void PutOnRoad(GameObject obj)
    {
        var x = obj.transform.position.x;
        var y = roadTop.y + obj.GetComponent<SpriteRenderer>().sprite.bounds.size.y / 2;
        obj.transform.position = new Vector3(x, y - 0.1f, 0);
    }


    public int RandomExcept(int min,int max,int except)
    {
        var res = except;
        while (res == except)
        {
            res = Random.Range(min,max);
        }

        return res;
    }

    public int RandomExceptName(int min, int max, int except, string[] exceptArr,GameObject[] arr)
    {
        List<int> alsoExcept = new List<int>();

        for(int i = 0; i < arr.Length; i++)
        {
            for (int j = 0; j < exceptArr.Length; j++)
            {
                if (arr[i].name == exceptArr[j])
                {
                    alsoExcept.Add(i);
                }
            }
        }
        var res = except;
        while (res == except || alsoExcept.Contains(res))
        {
            res = Random.Range(min, max);
        }

        return res;
    }
}
