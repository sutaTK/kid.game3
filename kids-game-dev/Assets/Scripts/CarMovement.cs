﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    [SerializeField] float movSpeed;

    private void FixedUpdate()
    {
        transform.position += Vector3.right * (movSpeed + (movSpeed * 0.2f)) * Time.deltaTime;
    }


    private void OnEnable()
    {
        GetComponent<WheelRotate>().enabled = true;
    }
}
