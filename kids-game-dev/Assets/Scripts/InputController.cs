﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Vector3 direction;
    public Vector3 mousePos;
    public bool isPressed
    {
        get
        {
            return Input.GetMouseButton(0) || Input.touchCount > 0;
        }
    }

    void Start()
    {
        Input.multiTouchEnabled = false;
    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            mousePos = Input.mousePosition;
        }
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                mousePos = Input.GetTouch(0).position;
            }
        }

    }
}
