﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBorders : MonoBehaviour
{
    public void DisableChildObject(string name)
    {
        for (int i = 0; i<transform.childCount;i++)
        {
            var obj = transform.GetChild(i).gameObject;
            if (obj.name.Equals(name))
            {
                obj.SetActive(false);
                return;
            }
        }
    }
}
