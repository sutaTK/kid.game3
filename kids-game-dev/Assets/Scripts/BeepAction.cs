﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeepAction : MonoBehaviour, IInteractiveAction
{
    public bool isBlocked = true;
    [SerializeField] AudioClip audio;

    private AudioSource audioSource;

    private void Start()
    {
        
    }
    public void Action()
    {
        //audioSource.clip = audio;
        //audioSource.Play();
    }

    public void TurnOff()
    {

    }

    public void TurnOn()
    {
        if (isBlocked)
            return;
        isBlocked = true;
        if(audioSource == null)
            audioSource = GetComponent<AudioSource>();
        audioSource.clip = audio;
        audioSource.Play();
    }
}
