﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobberWindowInteraction : MonoBehaviour,IInteractiveAction
{
    void IInteractiveAction.Action()
    {
        var catching = FindObjectOfType<CatchingRobbers>();
        var prison = FindObjectOfType<Prison>();
        var target = GetComponent<PartSettings>().targetObject;
        prison.FillWindow(target);
        catching.robbers.Remove(gameObject);

        if (catching.robbers.Count == 0)
        {
            catching.GetComponent<PoliceLevel>().LeavePrison();
        }
        Destroy(gameObject);
    }

    void IInteractiveAction.TurnOff()
    {
        throw new System.NotImplementedException();
    }

    void IInteractiveAction.TurnOn()
    {
        throw new System.NotImplementedException();
    }
    
}
