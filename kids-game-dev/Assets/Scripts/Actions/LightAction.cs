﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightAction : MonoBehaviour, IInteractiveAction
{
    [SerializeField] SpriteRenderer light;
    [SerializeField] AudioClip audio;

    private SpriteRenderer renderer;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
            audioSource = gameObject.AddComponent<AudioSource>();

        renderer = GetComponent<SpriteRenderer>();
    }

    public void Action()
    {
        StartCoroutine(Act());
    }

    public void TurnOff()
    {
        light.enabled = false;
    }

    public void TurnOn()
    {
        light.enabled = true;
    }

    IEnumerator Act()
    {
        audioSource.clip = audio;
        audioSource.Play();
        light.enabled = true;
        yield return new WaitForSeconds(0.5f);
        light.enabled = false;
        yield return new WaitForSeconds(0.5f);
        audioSource.Play();
        light.enabled = true;
        yield return new WaitForSeconds(0.5f);
        light.enabled = false;
    }
}
