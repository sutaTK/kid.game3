﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractiveAction
{
    void Action();

    void TurnOn();

    void TurnOff();
}
