﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWindowState 
{
    void Active(FillingWindow window);
}
