﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveBuildings : MonoBehaviour
{
    GameObject car;
    IInteractiveBuilding interactive;
    [SerializeField] float distanceToStop = 5f;
    public bool isTrigger=false;
    public bool hasRoad = true;

    void Awake() 
    {
        interactive = GetComponent<IInteractiveBuilding>();
    }
    void Start()
    {
        car = GameObject.Find("CarBase");
        LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        if (hasRoad) 
        {
            LvlGenerator gen = GetComponent<MoveEnviroment>().GetGenerator();
            gen.GenerateRoadInteractive(gameObject);
        }
        
        
    }

    void Update() 
    {
        if (gameObject.transform.position.x-car.transform.position.x <= distanceToStop  && isTrigger) 
        {
            LevelManager.isMoved = false;
            interactive.Action();
            isTrigger = false;
        }

    }
}
