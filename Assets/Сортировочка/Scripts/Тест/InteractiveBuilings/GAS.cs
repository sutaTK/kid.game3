﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GAS : MonoBehaviour, IInteractiveBuilding
{
    private GameObject tourist;
    private GameObject rider;
    private GameObject car;
    private GameObject pistol;
    private GameObject shlang;
    private LevelManager Level;
    private Animator playerAnim;
    [SerializeField] private Image imageDark;
    [SerializeField] private Image imageLight;

    [SerializeField] private FuelBtn fuelBtn;

    private bool isFull = false;
    [SerializeField] private GameObject money;

    [SerializeField] private Sprite[] spritesWithoutPistol;
    Sprite withPistolLight;
    Sprite withPistolDark;
    private GameObject music;
    void Start()
    {
        withPistolLight = imageLight.sprite;
        withPistolDark = imageDark.sprite;

        if (GetComponent<InteractiveBuildings>().isTrigger) fuelBtn = GameObject.Find("Fuel").GetComponent<FuelBtn>();
        Level = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        rider = GameObject.Find("Rider");
        car = Level.car;
        pistol = Level.GetPistolObj();
        shlang = Level.GetShlangObj();
        tourist = Level.character.gameObject;
        playerAnim = tourist.GetComponent<Animator>();
    }
    void IInteractiveBuilding.Action()
    {
        tourist.SetActive(true);
        rider.SetActive(false);
        music = GameObject.Find("SoundManager");
        music.GetComponent<AudioSource>().clip = null;
        StartCoroutine(WaitForCharStop(tourist.GetComponent<Character>()));
    }

    IEnumerator WaitForCharStop(Character charact)
    {
        charact.GoTo(pistol.transform.position);
        yield return new WaitWhile(() => charact.isMoving);
        playerAnim.Play("Idle");
        tourist.transform.localScale = new Vector3(80f, 80f, 1f);
        ShowPistol();
    }

    void ShowPistol() 
    {
        pistol.SetActive(true);
        StartCoroutine(WaitForStartRefill());
    }

    IEnumerator WaitForStartRefill()
    {
        yield return new WaitWhile(()=>!pistol.GetComponent<Pistol>().isWork);
        pistol.SetActive(false);
        ChangeAZSSprite(true);
        shlang.SetActive(true);
        Pistol pistolComp = pistol.GetComponent<Pistol>();
        pistol.transform.localPosition = pistolComp.spawnLocalPosition;
        pistolComp.isWork = false;
        StartCoroutine(MoneyTransform());
        StartCoroutine(EndInteractive());
        StartCoroutine(FillTheTank());
    }

    void ChangeAZSSprite(bool isWork) 
    {
        if (isWork)
        {
            imageDark.sprite = spritesWithoutPistol[0];
            imageLight.sprite = spritesWithoutPistol[1];
        }
        else 
        {
            imageDark.sprite = withPistolDark;
            imageLight.sprite = withPistolLight;
        }
    }

    IEnumerator EndInteractive() 
    {
        yield return new WaitWhile(()=>!isFull);
        ChangeAZSSprite(false);
        shlang.SetActive(false);
        playerAnim.Play("Walk");
        tourist.transform.localScale = new Vector3(68f, 68f, 1f);
        tourist.GetComponent<Character>().GoTo(car.transform.position);
        yield return new WaitWhile(() => tourist.GetComponent<Character>().isMoving);
        tourist.SetActive(false);
        rider.SetActive(true);
        music.GetComponent<SoundManager>().TurnRideMusic();
        LevelManager.isMoved = true;
        Unlock();
    }

    IEnumerator FillTheTank()
    {
        for (int i = FuelBtn.fuel; i<9; i++) 
        {
            yield return new WaitForSeconds(2f);
            FuelBtn.fuel = i;
            fuelBtn.ChangeSprite();
        }
        isFull = true;
    }

    IEnumerator MoneyTransform()
    {
        money.SetActive(true);
        Vector3 currentStart = money.transform.localPosition;
        Vector3 endPos = currentStart;
        endPos.y = 150f;

        for (float i = 0; i <= 1f; i += 0.02f)
        {
            yield return new WaitForSeconds(0.02f);
            money.transform.localPosition = Vector3.Lerp(currentStart, endPos, i);
        }

        money.SetActive(false);
    }

    void Unlock()
    {
        GameObject obj = Level.GetGameplayBtn();
        for (int i = 0; i < obj.transform.childCount; i++) 
        {
            obj.transform.GetChild(i).gameObject.SetActive(true);
        }
        fuelBtn.isFill = false;
        StartCoroutine(fuelBtn.FuelReduction());
    }

    
}
