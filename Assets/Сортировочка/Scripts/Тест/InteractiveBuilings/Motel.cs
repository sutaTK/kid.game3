﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motel : MonoBehaviour, IInteractiveBuilding
{
    private GameObject tourist;
    private GameObject rider;
    private GameObject car;
    private LevelManager Level;

    [SerializeField] private GameObject bed;
    [SerializeField] private GameObject money;
    [SerializeField] private GameObject Zzz;

    Vector3 posToGo;
    private GameObject music;

    void IInteractiveBuilding.Action()
    {
        Level = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        rider = GameObject.Find("Rider");
        car = Level.car;
        tourist = Level.character.gameObject;
        tourist.SetActive(true);
        rider.SetActive(false);
        music = GameObject.Find("SoundManager");
        music.GetComponent<AudioSource>().clip = null;
        StartCoroutine(WaitForCharStop(tourist.GetComponent<Character>()));
    }

    IEnumerator WaitForCharStop(Character charact)
    {
        posToGo = transform.position;
        posToGo.x -= 1f;
        charact.GoTo(posToGo);
        
        yield return new WaitWhile(()=>charact.isMoving);
        tourist.GetComponent<Animator>().Play("Idle");
        tourist.transform.localScale = new Vector3(80f, 80f, 1f);
        ShowBedCloud();
    }

    void ShowBedCloud() 
    {
        bed.SetActive(true);
    }

    public void CloudClicked() 
    {
        tourist.SetActive(false);
        StartCoroutine(MoneyTransform());
        bed.SetActive(false);
    }

    IEnumerator MoneyTransform() 
    {
        money.SetActive(true);
        Vector3 currentStart = money.transform.localPosition;
        Vector3 endPos = currentStart;
        endPos.y = 102f;

        for (float i = 0; i <= 1f; i += 0.02f) 
        {
            yield return new WaitForSeconds(0.02f);
            money.transform.localPosition = Vector3.Lerp(currentStart, endPos, i);
        }

        money.SetActive(false);
        StartCoroutine(WaitForSleep());
    }

    IEnumerator WaitForSleep() 
    {
        Zzz.SetActive(true);
        yield return new WaitForSeconds(5f);
        Zzz.SetActive(false);
        LevelManager.isDay = true;
        yield return new WaitForSeconds(2f);
        tourist.SetActive(true);
        tourist.GetComponent<Character>().GoTo(car.transform.position);
        tourist.transform.localScale = new Vector3(68f, 68f, 1f);
        yield return new WaitWhile(() => tourist.GetComponent<Character>().isMoving);
        tourist.SetActive(false);
        rider.SetActive(true);

        music.GetComponent<SoundManager>().TurnRideMusic();
        LevelManager.isMoved = true;
        Unlock();
    }

    void Unlock() 
    {
        GameObject obj = Level.GetGameplayBtn();
        obj.SetActive(true);
    }
}
