﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractions : MonoBehaviour, IInteractiveBuilding
{
    private GameObject tourist;
    private GameObject rider;
    private GameObject car;
    private LevelManager Level;
    private Animator playerAnim;
    [SerializeField] private GameObject photo;
    MissionProgress mission;
    private GameObject music = null;
    void Start() 
    {
        mission = GameObject.Find("LevelManager").GetComponent<MissionProgress>();
        Level = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        rider = GameObject.Find("Rider");
        car = Level.car;
        tourist = Level.character.gameObject;
        playerAnim = tourist.GetComponent<Animator>();
    }
    void IInteractiveBuilding.Action()
    {
        tourist.SetActive(true);
        rider.SetActive(false);
        music = GameObject.Find("SoundManager");
        music.GetComponent<AudioSource>().clip = null;
        StartCoroutine(WaitForCharStop(tourist.GetComponent<Character>()));
    }

    IEnumerator WaitForCharStop(Character charact)
    {
        charact.GoTo(this.transform.position);

        yield return new WaitWhile(() => charact.isMoving);
        playerAnim.Play("Idle");
        tourist.transform.localScale = new Vector3(80f, 80f, 1f);
        ShowPhotoCloud();
    }

    void ShowPhotoCloud()
    {
        photo.SetActive(true);
    }
    public void CloudClicked()
    {
        tourist.GetComponent<Animator>().Play("Interactive");
        photo.SetActive(false);
        StartCoroutine(WaitForEndPhoto());
    }

    IEnumerator WaitForEndPhoto() 
    {
        yield return new WaitForSeconds(6f);
        tourist.transform.localScale = new Vector3(68f, 68f, 1f);
        playerAnim.Play("Walk");
        tourist.GetComponent<Character>().GoTo(car.transform.position);
        yield return new WaitWhile(() => tourist.GetComponent<Character>().isMoving);
        tourist.SetActive(false);
        rider.SetActive(true);
        LevelManager.isMoved = true;

        music.GetComponent<SoundManager>().TurnRideMusic();
        
        Unlock();

        mission.TaskComplete();
    }

    void Unlock()
    {
        GameObject obj = Level.GetGameplayBtn();
        obj.SetActive(true);
    }
}
