﻿using UnityEngine;
using UnityEngine.UI;
public class Glowing : MonoBehaviour
{
    [SerializeField] Material Glow;
    private Image image;

    void Awake() 
    {
        image = GetComponent<Image>();
    }
    public void AddGlow() 
    {
        image.material = Glow;
    }

    public void RemoveGlow() 
    {
        image.material = null;
    }
}
