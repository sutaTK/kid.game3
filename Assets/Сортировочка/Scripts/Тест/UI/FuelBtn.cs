﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelBtn : MonoBehaviour
{
    [SerializeField] private List<Sprite> sprites = new List<Sprite>();
    [SerializeField] float[] timeForReduce = new float[3] { 9f, 8f, 4f };
    [SerializeField] AZSBtn azs;
    public static int fuel = 8;
    public bool isFill = false;

    private bool isFlash;
    [SerializeField] private List<Sprite> spriteFlash = new List<Sprite>();

    private Image image;

    void Awake() 
    {
        image = GetComponent<Image>();
    }
    void Start() 
    {
        isFill = false;
        if (Settings.difficult == Settings.Difficult.Easy)
            fuel = 8;
        else if (Settings.difficult == Settings.Difficult.Medium)
            fuel = 6;
        else
            fuel = 3;
    }

    public void FuelBtnClick() 
    {
        if (fuel<8 && !isFill) 
        {
            isFill = true;
            azs.OnBtnClicked();
            StopAllCoroutines();
        }
    }

    public IEnumerator FuelReduction()
    {
        for (int i = fuel; i != 0; i--)
        {
            fuel = i;
            image.sprite = sprites[fuel];
            yield return new WaitForSeconds(timeForReduce[LevelManager.lvlSpeed]);
            if (!LevelManager.isMoved)
                break;
            Debug.LogWarning(fuel);
        }
        azs.OnBtnClicked();
    }

    IEnumerator Flash()
    {
        while (isFlash) 
        {
            image.sprite = spriteFlash[fuel];
            yield return new WaitForSeconds(2f);
            image.sprite = sprites[fuel];
        }
    }

    public void ChangeSprite() 
    {
        image.sprite = sprites[fuel];
    }
    void OnEnable() 
    {
        StartCoroutine(FuelReduction());
        image.sprite = sprites[fuel];
    }

}
