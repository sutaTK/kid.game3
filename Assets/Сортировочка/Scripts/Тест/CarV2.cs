﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CarV2 : MonoBehaviour, IDropHandler
{
    [SerializeField] LevelManager manager;
    public ItemSlot[] items { get; private set; }

    private WheelRotate wheels;
    private AudioSource audioSource;
    private bool SpeedUp = false;
    private bool SpeedDown = false;
    [SerializeField] private AudioClip[] motorSounds = new AudioClip[3];
    [SerializeField] private GameObject carObj;
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        items = gameObject.GetComponentsInChildren<ItemSlot>();
        wheels = GetComponent<WheelRotate>();
    }

    void Start()
    {
        ChangeMotorSound(0);
        DifficultCheck();
    }
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.name == "Pistol")
        {
            eventData.pointerDrag.GetComponent<Pistol>().isWork = true;
        }
    }
    public void CheckForCarAvaible()
    {
        bool isFull = true;
        foreach (ItemSlot itemSlot in items)
        {
            if (itemSlot.isEmpty)
            {
                isFull = false;
                break;
            }
        }
        Debug.LogWarning(isFull);
        if (isFull)
        {
            LevelManager.isStart = true;
            //touriste.GetComponent<Animator>().Play("Walk");
        }
    }

    private void DifficultCheck()
    {
        if (Settings.difficult == Settings.Difficult.Hard)
        {
            foreach (ItemSlot itemSlot in items) 
            {
                Image image = itemSlot.GetComponent<Image>();
                var c = image.color;
                c.a = 0;
                image.color = c;
            }
        }
    }

    public void CheckDisableDetailTriggers(Detail detail)
    {
        foreach (ItemSlot item in items)
        {
            Image image = item.gameObject.GetComponent<Image>();
            image.raycastTarget = item.nameOfCorrectDetail == detail.nameOfDetail && item.isEmpty;
        }
    }
    private void FixedUpdate()
    {
        if (LevelManager.isMoved)
        {
            wheels.enabled = true;
            audioSource.mute = false;
        }
        else 
        {
            wheels.enabled = false;
            audioSource.mute = true;
        }

        if (SpeedUp)
            carObj.transform.position += Vector3.right * .5f * Time.deltaTime;

        else if (SpeedDown)
            carObj.transform.position += Vector3.left * 1f * Time.deltaTime;

    }

    public void ChangeMotorSound(int soundIndx) 
    {
        audioSource.clip = motorSounds[soundIndx];
        audioSource.Play();
    }

    public IEnumerator MoveForward() 
    {
        SpeedUp = true;
        yield return new WaitForSeconds(.5f);
        SpeedUp = false;
    }

    public IEnumerator MoveBack()
    {
        SpeedDown = true;
        yield return new WaitForSeconds(.5f);
        SpeedDown = false;
    }
}
