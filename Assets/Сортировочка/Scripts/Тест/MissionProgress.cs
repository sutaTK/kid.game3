﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionProgress : MonoBehaviour
{
    [SerializeField] private float hardNumOfInteractive = 10.0f;
    [SerializeField] private float mediumNumOfInteractive = 7.0f;
    [SerializeField] private float easyNumOfInteractive = 4.0f;
    [SerializeField] private static BaloonGenerator baloonGenerator;

    [SerializeField] private SoundManager soundManager;
    [SerializeField] private Slider progressBar;

    public float targetProgress;
    private float fillSpeed = 1f;
    void Start() 
    {

        if (Settings.difficult == Settings.Difficult.Easy)
            progressBar.maxValue = 3.0f;
        else if (Settings.difficult == Settings.Difficult.Medium)
            progressBar.maxValue = 7.0f;
        else if (Settings.difficult == Settings.Difficult.Hard)
            progressBar.maxValue = 9.0f;

        baloonGenerator = FindObjectOfType<BaloonGenerator>();
        baloonGenerator.enabled = false;
        
        //progressBar.maxValue = maxProgress;
    }

    void Update() 
    {
        if (progressBar.value < targetProgress)
            progressBar.value += fillSpeed * Time.deltaTime;
        else if (progressBar.value > targetProgress)
            progressBar.value -= fillSpeed * Time.deltaTime;
    }

    public void TaskComplete() 
    {
        targetProgress = targetProgress+1.0f;
        if (targetProgress >= progressBar.maxValue)
        {
            LevelManager.isMoved = false;
            Victory();
        }
        
    }

    private void Victory() 
    {
        StartCoroutine(GetComponent<LevelManager>().RestartLvl());
        soundManager.TurnVictoryMusic();
        baloonGenerator.enabled = true;
        FuelBtn.fuel = 8;
    }


}
