﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public enum DetailSoundType { Metall, Glass, Police, Sportcar ,None };

    public static List<AudioClip> DetailSoundsPickup = new List<AudioClip>();
    public static List<AudioClip> DetailSoundsPut = new List<AudioClip>();
    public static AudioClip DetailError;
    [SerializeField] private AudioClip garageMusic;
    [SerializeField] private AudioClip engineStart;
    [SerializeField] private AudioClip startRide;
    [SerializeField] private AudioClip victoryMusic;
    [SerializeField] private List<AudioClip> _DetailSoundsPickup = new List<AudioClip>();
    [SerializeField] private List<AudioClip> _DetailSoundsPut = new List<AudioClip>();
    [SerializeField] private AudioClip _DetailError;

    void Start() 
    {
        InitializeDetailSounds();
        GetComponent<AudioSource>().clip = garageMusic;
    }

    private void InitializeDetailSounds() 
    {
        DetailSoundsPickup = new List<AudioClip>(_DetailSoundsPickup);
        _DetailSoundsPickup.Clear();

        DetailSoundsPut = new List<AudioClip>(_DetailSoundsPut);
        _DetailSoundsPut.Clear();

        DetailError = _DetailError;
        _DetailError = null;
    }

    public AudioSource GetLvlAudioSource() 
    {
       return GetComponent<AudioSource>();
    }

    public IEnumerator AudioDecay()
    {
        AudioSource audio = GetComponent<AudioSource>();
        for (float i = 1; i > 0; i -= 0.05f)
        {
            audio.volume = i;
            yield return new WaitForSeconds(0.3f);
        }    
    }
    public IEnumerator StartGameplayMusic() 
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = engineStart;
        audio.volume = 1;
        audio.Play();
        yield return new WaitForSeconds(2f);
        TurnRideMusic();
    }

    public void TurnRideMusic() 
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = startRide;
        audio.Play();
    }


    public void TurnVictoryMusic()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.loop = false;
        audio.clip = victoryMusic;
        audio.Play();
    }
}
